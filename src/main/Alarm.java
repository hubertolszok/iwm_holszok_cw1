package main;

public class Alarm extends Packet {
	public int channelNr;
	public int threshold;
	public int direction;

	public Alarm(int channelNr, int threshold, int direction) {
		this.channelNr = channelNr;
		this.threshold = threshold;
		this.direction = direction;
	}

	public Alarm() {
		this.channelNr = 1;
		this.threshold = 10;
		this.direction = 1;
	}

	public String ToString() {
		String text;

		text = Integer.toString(this.channelNr) + " " + Integer.toString(this.threshold) + " " + Integer.toString(this.direction);

		return text;
	}

}
