package main;

public class TimeHistory extends Sequence {
	public double sensitivity;

	public TimeHistory(double sensitivity) {
		this.sensitivity = sensitivity;
	}

	public TimeHistory() {
		this.sensitivity = 100;
	}

	public String ToString() {
		String text;

		text = Double.toString(sensitivity);

		return text;
	}

}
