package main;

public class Spectrum extends Sequence {
	public int scaling;

	public Spectrum(int scaling) {
		this.scaling = scaling;
	}

	public Spectrum() {
		this.scaling = 10;
	}

	public String ToString() {
		String text;

		text = Integer.toString(scaling);

		return text;
	}

}
